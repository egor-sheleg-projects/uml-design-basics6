﻿// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable IntroduceOptionalParameters.Global
#pragma warning disable S107 // Methods should not have too many parameters

namespace UmlDesignBasics
{
    public class SimpleClass13
    {
        public SimpleClass13()
            : this(1087)
        {
            this.IntValue = 1087;
            this.LongValue = 10482;
            this.FloatValue = -10387.05832f;
            this.DoubleValue = -10382.73521;
            this.CharValue = 'h';
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue)
            : this(intValue, 10482L)
        {
            this.IntValue = intValue;
            this.LongValue = 10482;
            this.FloatValue = -10387.05832f;
            this.DoubleValue = -10382.73521;
            this.CharValue = 'h';
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue)
            : this(intValue, longValue, -10387.05832f)
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = -10387.05832f;
            this.DoubleValue = -10382.73521;
            this.CharValue = 'h';
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue)
            : this(intValue, longValue, floatValue, -10382.73521)
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = -10382.73521;
            this.CharValue = 'h';
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue, double doubleValue)
            : this(intValue, longValue, floatValue, doubleValue, 'h')
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = doubleValue;
            this.CharValue = 'h';
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue, double doubleValue, char charValue)
            : this(intValue, longValue, floatValue, doubleValue, charValue, false)
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = doubleValue;
            this.CharValue = charValue;
            this.BooleanValue = false;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue, double doubleValue, char charValue, bool boolValue)
            : this(intValue, longValue, floatValue, doubleValue, charValue, boolValue, "ghimnojkl")
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = doubleValue;
            this.CharValue = charValue;
            this.BooleanValue = boolValue;
            this.StringValue = "ghimnojkl";
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue, double doubleValue, char charValue, bool boolValue, string stringValue)
            : this(intValue, longValue, floatValue, doubleValue, charValue, boolValue, stringValue, null)
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = doubleValue;
            this.CharValue = charValue;
            this.BooleanValue = boolValue;
            this.StringValue = stringValue;
            this.ObjectValue = null;
        }

        public SimpleClass13(int intValue, long longValue, float floatValue, double doubleValue, char charValue, bool boolValue, string stringValue, object objectValue)
        {
            this.IntValue = intValue;
            this.LongValue = longValue;
            this.FloatValue = floatValue;
            this.DoubleValue = doubleValue;
            this.CharValue = charValue;
            this.BooleanValue = boolValue;
            this.StringValue = stringValue;
            this.ObjectValue = objectValue;
        }

        public int IntValue { get; private set; }

        public long LongValue { get; private set; }

        public float FloatValue { get; private set; }

        public double DoubleValue { get; private set; }

        public char CharValue { get; private set; }

        public bool BooleanValue { get; private set; }

        public string StringValue { get; private set; }

        public object ObjectValue { get; private set; }
    }
}
